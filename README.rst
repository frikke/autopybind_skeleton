Generating Wrapping Files for Drake
====================================

This repository is a skeleton which allows one to quickly set up an
environment which can use the AutoPyBind11 tool to generate PyBind11 code.

Setup
+++++

**Assumptions**:

* `Python 3.*`_ has been installed
* Drake_ has been installed

YAML Creation
-------------

The key to the AutoPyBind11 framework is the input YAML file
which dictates what C++ objects are to be wrapped by the tool.
See the README_ of AutoPyBind11 for the current list of objects
that can be wrapped and how to place them into the YAML file.

An example YAML file for two Drake objects can be found below:

.. code-block:: YAML

    drake:
      classes:
        RandomGenerator:
          file: "drake/common/random.h"
          inst: []
          customization:
            name: RandomGenerator
      enums:
        ToleranceType:
          file: "drake/common/constants.h"
          customization:
           name: ToleranceType
            custom_enum_vals:
              kAbsolute: absolute
              kRelative: relative

CMake Execution
---------------

As with the AutoPyBind11 repository, we recommend that Python's Virtual
Environment module be used.  This will allow installs and updates to not have
an effect on the system level packages.

If necessary, install virtualenv first.

If you're on Ubuntu:

.. code-block:: shell

  sudo apt install python3-venv

If you are OK with packages leaking into your ``$HOME`` environment:

.. code-block:: shell

  python3 -m pip install virtualenv

Then execute the module to create the virtual environment and activate it.

.. code-block:: shell

  python3 -m venv apb_env
  source apb_env/bin/activate

To set up the repository for use, first create an empty directory to use as
a build directory. Then enter that directory and execute ``cmake``.
The argument to `cmake` should be the path to this directory.

.. code-block:: shell

    mkdir autobind_build
    cd autobind_build
    cmake ../drake_example -DCMAKE_PREFIX_PATH=/path/to/drake

CMake will download AutoPyBind11 and other necessary programs in addition to
searching for the assumed programs above.  If those programs are not found, an
error will be printed to the screen.  CMake will also attempt to install the
AutoPyBind11 code via PIP.

If the CMake process finishes without an error, the files can be generated.
For Linux environments, this is as simple as running make from the "build"
directory:

.. code-block:: shell

    make

Reviewing Output
++++++++++++++++

Once the files have been generated successfully, the written PyBind11 files
can be found in the build directory as well.
Firstly, there will be a ``<module>.cpp`` file which contains the main entry
point
for the library's C++ code.  There will also be a  ``free_functions_py.cpp``
should be found to capture those types of C++ objects not found in a namespace
or class.
Each C++ object (Namespace, class, enumeration ...) from the YAML file should
be found as ``<object_name>_py.cpp``.

From our above YAML example, we would expect to see the following files in
the build directory:

* ``gen_only_module.cpp``
* ``free_functions_py.cpp``
* ``drake_py.cpp``
* ``RandomGenerator_py.cpp``

.. _README: https://gitlab.kitware.com/autopybind11/autopybind11/-/blob/master/README.rst
.. _`Python 3.*`: https://www.python.org/
.. _Drake: https://github.com/RobotLocomotion/drake